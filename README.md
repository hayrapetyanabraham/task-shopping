<<<<<<< README.md
# task_shopping

A new Flutter project for managing shopping tasks.

## Architecture
The application follows the Model-View-ViewModel (MVVM) architecture pattern. This pattern separates the user interface (View) from the application logic and data (ViewModel), which in turn communicates with the model layer.

## Dependencies
The application uses several open-source packages from the Flutter community, including:

- [ ] sqflite: A plugin for SQLite databases.
- [ ] path: Provides common path operations.
- [ ] flutter_bloc: A state management library.
- [ ] auto_route: A routing library for Flutter.
- [ ] get_it: A simple Service Locator for Dart and Flutter projects.
- [ ] flutter_rounded_date_picker: A Flutter package for picking a date or a range of dates.
- [ ] intl: A library for internationalization and localization.
- [ ] fl_chart: A Flutter library for drawing beautiful charts.
- [ ] mocktail: A testing library for Dart and Flutter.


## Getting Started
Clone the repository: git clone https://github.com/<username>/<repository-name>.git.
Install the dependencies by running flutter pub get in the project directory.
Run the app using flutter run or using your IDE.

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.