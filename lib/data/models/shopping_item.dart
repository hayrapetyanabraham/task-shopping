import 'package:task_shopping/core/const/shopping_consts.dart';

class ShoppingItem {
  final int id;
  final String name;
  final DateTime purchaseTime;
  final double price;
  final int quantity;
  final DateTime warrantyPeriod;

  ShoppingItem({
    required this.id,
    required this.name,
    required this.purchaseTime,
    required this.price,
    required this.quantity,
    required this.warrantyPeriod,
  });

  Map<String, dynamic> toMap() {
    return {
      ShoppingConsts.columnId: id,
      ShoppingConsts.columnName: name,
      ShoppingConsts.columnPurchaseTime: purchaseTime.toIso8601String(),
      ShoppingConsts.columnPrice: price,
      ShoppingConsts.columnQuantity: quantity,
      ShoppingConsts.columnWarrantyPeriod: warrantyPeriod.toIso8601String(),
    };
  }

  factory ShoppingItem.fromMap(Map<String, dynamic> map) {
    return ShoppingItem(
      id: map[ShoppingConsts.columnId],
      name: map[ShoppingConsts.columnName],
      purchaseTime: DateTime.parse(map[ShoppingConsts.columnPurchaseTime]),
      price: map[ShoppingConsts.columnPrice],
      quantity: map[ShoppingConsts.columnQuantity],
      warrantyPeriod: DateTime.parse(map[ShoppingConsts.columnWarrantyPeriod]),
    );
  }
}
