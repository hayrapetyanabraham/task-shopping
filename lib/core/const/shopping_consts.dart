class ShoppingConsts {
  static const tableName = 'shopping_items';
  static const columnId = 'id';
  static const columnName = 'name';
  static const columnPurchaseTime = 'purchase_time';
  static const columnPrice = 'price';
  static const columnQuantity = 'quantity';
  static const columnWarrantyPeriod = 'warranty_period';
}
